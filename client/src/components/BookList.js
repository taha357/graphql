import React, { Component } from 'react';
import { gql } from 'apollo-boost';
import { graphql } from 'react-apollo';

const getBooksQuery = gql`
    {
        books{
            name
            id
        }
    }
`;

class BookList extends Component {
    getLists(){
        var books = this.props.data.books;
        if(this.props.data.loading)
            return (<li>Loading...</li>);

        return books.map((b) => {
            return (
                <li key={b.id}>{b.name}</li>
            );
        });
    }
    render() {
        return (
            <div>
                <ul id="book-list">
                    { this.getLists() }
                </ul>
            </div>
        );
    }
}

export default graphql(getBooksQuery)(BookList);
