const express = require('express');
const graphHTTP = require('express-graphql');
const mongoose = require('mongoose');
const schema = require('./schema/schema');
const cors = require('cors');

const app = express();

app.use(cors());

mongoose.connect('mongodb://taha:secret357@ds159563.mlab.com:59563/gql', { useNewUrlParser: true });

app.get('/', (req, res) => {
    console.log('here');
    res.send({done: 1});
});

app.use('/graphql', graphHTTP({
    schema,
    graphiql: true
}));



app.listen(4000, () => {
    console.log('Started Listening on 4000');
});